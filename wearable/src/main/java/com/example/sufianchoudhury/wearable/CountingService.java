package com.example.sufianchoudhury.wearable;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Sufian Choudhury on 13/12/2015.
 */
public class CountingService extends IntentService {

    public static final String REPORT_KEY = "REPORT_KEY";
    public static final String INTENT_KEY = "com.example.sufianchoudhury.wearable.BROADCAST";

    public CountingService() {
        super("BackgroundCounting");

}
    @Override
    protected void onHandleIntent(Intent intent) {

        int count = 0;
        while (count < 10) {
            synchronized (this) {
                try {
                    wait(1000);
                    count++;
                    Log.d(MainActivity.DEBUG_KEY, Integer.toString(count));
                } catch (Exception e) {
                }
            }
        }
        Log.d(MainActivity.DEBUG_KEY, "service finished");

    }
}