package com.example.sufianchoudhury.welcome;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by Sufian Choudhury on 07/12/2015.
 */
public class Welcome extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome);

    }
}
